console.log(fetch(`https://jsonplaceholder.typicode.com/todos`))


fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(response => response.json())
.then(response => {
   // console.log(response)
    response.map(element => {
        console.log(element.title);
 
        
    })
})

fetch(`https://jsonplaceholder.typicode.com/todos/10`,)
.then(res => res.json())
.then(res => {
        console.log(`The item illo est ratione doloremque quia maiores aut on the list has a status of true`);

    
})

fetch(`https://jsonplaceholder.typicode.com/todos`,{
    method: "POST",
    headers: {
        "Content-Type" :"application/json"
    },
    body: JSON.stringify({
        title:"New Post",
        body: "New Body",
        userID: 1
    })
})
.then( response => response.json())
.then ( response =>{
    console.log(response);

})

fetch(`https://jsonplaceholder.typicode.com/todos/7`, {
    method:"PUT",
    headers: {
        "Content-Type" :"application/json"
    },
    body: JSON.stringify({
                title:"Updated Post",
                description: "Updated description",
                status: "Updated Status",
                dateCompleted: "Updated Date Completed",
                userID: "Updated User ID"
     })
})
.then(res => res.json())
.then(res => {
    console.log(res);

})

fetch(`https://jsonplaceholder.typicode.com/todos/8`, {
    method:"PATCH",
    headers: {
                "Content-Type" :"application/json"
    },
    body: JSON.stringify({
                status:"Complete",
                date: "07/03/2022"
                
    })
})

.then (data => data.json())
.then(data => {
    console.log(data);
 
})

fetch(`https://jsonplaceholder.typicode.com/todos/9`,{
    method:"DELETE"

})